/* TOPNAV CODE */

/* A function is a block of code designed to perform a particular task(set of instructions) */
/* Here, we have defined a function named "myFunction()" without any parameters */
function myFunction() {

  /* var keyword is used for global scope and let keyword is used for function scope. */
  /* Here, x is a variable, declared with the var keyword. */
  /* We are accessing the element whose id = myTopnav and storing its value in variable x */
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}


/* SIDENAV CODE */

/* Here, we have defined a function named "openNav()" without any parameters */
function openNav() {
  /* Here, we are accessing the element whose id = mySidenav and changing its width */
  document.getElementById("mySidenav").style.width = "250px";
}

/* Here, we have defined a function named "closeNav()" without any parameters */
function closeNav() {
  /* Here, we are accessing the element whose id = mySidenav and changing its width */
  document.getElementById("mySidenav").style.width = "0";
}


/* SLIDESHOW CODE */

/* 3 - Create a flag named "slideIndex". It indicates the current slide. */
let slideIndex = 1;
/* 2 - Call the function created in Step 1 and pass the flag "slideIndex". */
showSlides(slideIndex);

/* 5 - Create a function to go to the previous slide or the next slide. */
function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

/* 1 - Define a function named "showSlides()" with parameter "n" to receive the flag. */
function showSlides(n) {
  let i;
  /* Select all the slides(3 div elements) and put them in a new variable "slides". An array is created that stores the div elements. */
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");

  /* Resets the flag to 1. From 3rd slide to 1st slide. */
  if (n > slides.length) {slideIndex = 1}
  /* Resets the flag to 3. From 1st slide to 3rd slide. */
  if (n < 1) {slideIndex = slides.length}
  /* To hide all the slides. */
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  /* Makes the passed slide visible. */
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
